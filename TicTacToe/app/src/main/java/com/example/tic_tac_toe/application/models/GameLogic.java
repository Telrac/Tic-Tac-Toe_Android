package com.example.tic_tac_toe.application.models;

import com.example.tic_tac_toe.R;
import com.example.tic_tac_toe.utils.Players;

/**
 * Contient la logique de jeu de l'application.
 */
public class GameLogic {
    public GameLogic(GameData gameData){
        this.gameData = gameData;
    }
    private final GameData gameData;

    /**
     * Place une marque aux coordonnées mises en paramètre.
     * @param x la coordonnée x de la case
     * @param y la coordonnée y de la case
     */
    public int placeMark(int x, int y){
        Players mark = gameData.isPlayer1Turn()? Players.Player1 : Players.Player2;
        //Une case ne devrait pas être marquée si la partie est terminée ou si elle est déjà marquée.
        if(gameData.isGameOver()){
            return R.string.snack_game_over;
        }
        else if(gameData.getGameGrid()[x][y] != Players.NO_ONE){
            return R.string.snack_not_free;
        }
        gameData.markGameGrid(mark, x, y);

        if(checkIfGameOver()){
            gameData.setGameOver(true);
        }
        else{
            gameData.changeTurn();
        }
        return -1;
    }

    /**
     * Vérifie si la partie est terminée.
     * @return Si la partie est terminée ou non.
     */
    private boolean checkIfGameOver(){
        Players winner = Players.NO_ONE;
        //Check horizontal
        for (int i = 0; i < gameData.getGameGrid().length; i++) {
            Players mark = gameData.getGameGrid()[i][0];
            if(mark == Players.NO_ONE){
                continue;
            }

            for (int j = 1; j < gameData.getGameGrid()[0].length; j++) {
                if(mark != gameData.getGameGrid()[i][j]){
                    break;
                }
                else if(j == gameData.getGameGrid()[0].length -1){
                    winner = mark;
                    break;
                }
            }
            if(checkIfIsWinner(winner)){
                return true;
            }
        }

        //Check vertical
        for (int i = 0; i < gameData.getGameGrid().length; i++) {
            Players mark = gameData.getGameGrid()[0][i];
            if(mark == Players.NO_ONE){
                continue;
            }

            for (int j = 1; j < gameData.getGameGrid()[0].length; j++) {
                if(mark != gameData.getGameGrid()[j][i]){
                    break;
                }
                else if(j == gameData.getGameGrid()[0].length -1){
                    winner = mark;
                }
            }
            if(checkIfIsWinner(winner)){
                return true;
            }
        }

        //Check diagonale gauche
        Players mark = gameData.getGameGrid()[0][0];
        for (int i = 1; i < gameData.getGameGrid().length; i++) {
            if(mark != gameData.getGameGrid()[i][i]){
                break;
            }
            else if(i == gameData.getGameGrid()[0].length -1){
                winner = mark;
            }
        }
        if(checkIfIsWinner(winner)){
            return true;
        }

        //Check diagonale droite
        mark = gameData.getGameGrid()[gameData.getGameGrid().length-1][0];
        for (int i = 0, j = gameData.getGameGrid().length-1; i < gameData.getGameGrid().length ; i++, j--) {
            if(mark != gameData.getGameGrid()[i][j]){
                break;
            }
            else if(i == 0){
                winner = mark;
            }
        }
        if(checkIfIsWinner(winner)){
            return true;
        }
        else if(gameData.getMarks() == 9){
            gameData.setWinner(Players.NO_ONE);
            return true;
        }
        return false;
    }

    /**
     * Vérifie si l'élément mis en paramètre est gagnant.
     * @param winner
     * @return
     */
    private boolean checkIfIsWinner(Players winner){
        if(winner != Players.NO_ONE){
            gameData.setWinner(winner);
            return true;
        }
        return false;
    }
}
