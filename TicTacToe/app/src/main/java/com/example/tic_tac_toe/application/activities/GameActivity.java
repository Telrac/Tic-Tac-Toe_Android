package com.example.tic_tac_toe.application.activities;

import com.example.tic_tac_toe.application.models.GameData;
import com.example.tic_tac_toe.application.models.GameLogic;
import com.example.tic_tac_toe.application.view_models.GameViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.tic_tac_toe.R;
import com.example.tic_tac_toe.databinding.ActivityGameBinding;
import com.example.tic_tac_toe.resources.ResourcesGetter;
import com.example.tic_tac_toe.utils.Constants;
import com.example.tic_tac_toe.utils.events.EventObserver;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

/**
 * Activité principale de l'application.
 */
public class GameActivity extends AppCompatActivity {
    GameViewModel viewModel;
    GameData gameData;
    GameLogic gameLogic;
    /**
     * Permet la liaison entre le view model et la vue
     */
    ActivityGameBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Si le model a été sauvegardé, on récupère ses données.
        if (savedInstanceState != null){
            gameData = savedInstanceState.getParcelable(Constants.GAME_MODEL);
        }
        //Sinon, on en crée un nouveau.
        else{
            gameData = new GameData();
        }
        gameLogic = new GameLogic(gameData);

        viewModel = new GameViewModel(gameData, gameLogic, new ResourcesGetter(getApplication()));

        //Liaison entre le view model et la vue.
        binding = DataBindingUtil.setContentView(this, R.layout.activity_game);
        binding.setViewModel(viewModel);

        //Permet d'obtenir un évènement contenant un message à afficher à partir du view model.
        viewModel.getSnackBarEvent().observe(this, new EventObserver<>(this::makeSnackBar));
    }

    /**
     * Affiche un snackBar.
     * @param message Le message à utiliser dans le snackBar
     */
    protected void makeSnackBar(String message){
        Snackbar.make(findViewById(android.R.id.content), message, BaseTransientBottomBar.LENGTH_LONG).show();
    }

    /**
     * Sauvegarde l'état des données à travers les différentes étape du cycle de vie de l'activité.
     * @param bundle
     */
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable(Constants.GAME_MODEL, gameData);
    }
}