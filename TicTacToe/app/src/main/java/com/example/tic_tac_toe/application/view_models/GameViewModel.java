package com.example.tic_tac_toe.application.view_models;

import android.graphics.drawable.Drawable;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.example.tic_tac_toe.BR;
import com.example.tic_tac_toe.application.models.GameLogic;
import com.example.tic_tac_toe.utils.Players;
import com.example.tic_tac_toe.application.models.GameData;
import com.example.tic_tac_toe.R;
import com.example.tic_tac_toe.resources.Resources;
import com.example.tic_tac_toe.utils.events.Event;

/**
 * View Model du jeu.
 */
public class GameViewModel extends BaseObservable {

    public GameViewModel(GameData gameData, GameLogic gameLogic, Resources resources){
        this.gameData = gameData;
        this.gameLogic = gameLogic;
        this.resources = resources;
        cross = resources.getDrawable(R.drawable.cross);
        circle = resources.getDrawable(R.drawable.circle);
        snackBarEvent = new MutableLiveData<>();
        notifyPropertyChanged(BR.gameState);
    }
    private final GameData gameData;
    private final GameLogic gameLogic;
    private final Resources resources;
    private final Drawable cross;
    private final Drawable circle;
    protected MutableLiveData<Event<String>> snackBarEvent;

    public MutableLiveData<Event<String>> getSnackBarEvent() {
        return snackBarEvent;
    }


    /**
     * Obtient la grille de jeu.
     */
    public Drawable getGridElement(int x, int y){
        Players player = gameData.getGameGrid()[x][y];
        if(player == Players.NO_ONE){
            return null;
        }
        return player == Players.Player1? cross: circle;
    }

    @Bindable
    /**
     * Obtient le message de l'état du jeu.
     */
    public String getGameState(){
        if(gameData.isGameOver()){
            return getWinMessage();
        }
        String player = gameData.isPlayer1Turn()? "1": "2";
        return resources.getString(R.string.msg_player_turn, player);
    }

    /**
     * Obtient le message de victoire (ou de match nul).
     */
    public String getWinMessage(){
        if(gameData.getWinner() == Players.NO_ONE){
            return resources.getString(R.string.msg_player_tie);
        }
        String player = gameData.getWinner() == Players.Player1? "1": "2";
        return resources.getString(R.string.msg_player_won, player);
    }

    /**
     * Action d'appuyer sur une case de la grille de jeu.
     * @param x la coordonnée x de la case
     * @param y la coordonnée y de la case
     */
    public void onMark(int x, int y){
        int messageCode = gameLogic.placeMark(x, y);
        if(messageCode != -1){
            snackBarEvent.postValue(new Event<>(resources.getString(messageCode)));
        }
        notifyPropertyChanged(BR._all);
    }

    /**
     * Action de réinitialiser le jeu.
     */
    public void onReset(){
        gameData.resetGame();
        notifyPropertyChanged(BR._all);
    }

}
