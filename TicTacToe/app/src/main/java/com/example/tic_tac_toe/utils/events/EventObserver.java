package com.example.tic_tac_toe.utils.events;

import androidx.lifecycle.Observer;

/**
 * Observeur permettant à une activité d'obtenir des instructions du view model.
 * Source: https://stackoverflow.com/a/56072658
 */
public class EventObserver<T> implements Observer<Event<? extends T>> {

    public interface EventUnhandledContent<T> {
        void onEventUnhandledContent(T t);
    }

    private final EventUnhandledContent<T> content;

    public EventObserver(EventUnhandledContent<T> content) {
        this.content = content;
    }

    @Override
    public void onChanged(Event<? extends T> event) {
        if (event != null) {
            T result = event.getContentIfNotHandled();
            if (result != null && content != null) {
                content.onEventUnhandledContent(result);
            }
        }
    }
}
