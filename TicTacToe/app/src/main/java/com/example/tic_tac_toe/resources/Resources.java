package com.example.tic_tac_toe.resources;

import android.graphics.drawable.Drawable;
/**
 * Interface des méthodes permettant d'accéder aux ressources d'Android.
 */
public interface Resources {
    String getString(int id);
    String getString(int id, String param);
    Drawable getDrawable(int id);
}
