package com.example.tic_tac_toe.utils;

import java.io.Serializable;

/**
 * Enumération représentant les différents joueurs.
 */
public enum Players implements Serializable {
    NO_ONE,
    Player1,
    Player2
}
