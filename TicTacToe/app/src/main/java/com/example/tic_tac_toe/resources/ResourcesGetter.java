package com.example.tic_tac_toe.resources;

import android.app.Application;
import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Objet permettant d'accéder aux différentes ressources d'Android.
 */
public class ResourcesGetter implements Resources {
    private final Context context;

    /**
     * Constructeur de l'accesseur des ressources d'Android.
     * @param application Instance de l'application. Ce paramètre est utilisé pour obtenir un contexte
     *                    qui persiste entre les différentes étapes du cycle de vie de l'application.
     */
    public ResourcesGetter(Application application){
        context = application.getApplicationContext();
    }
    @Override
    public String getString(int id){
        return context.getString(id);
    }
    @Override
    public String getString(int id, String param){
        return context.getString(id, param);
    }
    @Override
    public Drawable getDrawable(int id){
        return context.getDrawable(id);
    }


}
