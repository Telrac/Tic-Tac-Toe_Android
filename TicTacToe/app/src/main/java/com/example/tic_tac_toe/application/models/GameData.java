package com.example.tic_tac_toe.application.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.example.tic_tac_toe.utils.Players;

/**
 * Contient les données du jeu.
 */
public class GameData implements Parcelable {
    private boolean isPlayer1Turn;
    private final Players[][] gameGrid;
    private int marks;
    private Players winner;
    private boolean gameOver;

    /**
     * Constructeur du model du jeu.
     */
    public GameData(){
        gameGrid = new Players[3][3];
        resetGame();
    }

    public boolean isPlayer1Turn() {
        return isPlayer1Turn;
    }

    public Players[][] getGameGrid() {
        return gameGrid;
    }

    public Players getWinner() {
        return winner;
    }

    public int getMarks() {
        return marks;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setWinner(Players winner) {
        this.winner = winner;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    /**
     * Place une marque sur la grille aux coordonnées mises en paramètre.
     * @param mark La marque à placer.
     * @param x La coordonnée X.
     * @param y La coordonnée Y.
     */
    public void markGameGrid(Players mark, int x, int y){
        gameGrid[x][y] = mark;
        ++marks;
    }

    /**
     * Réinitialise les cases de la grille de jeu.
     */
    private void resetGameGrid(){
        for (int i = 0; i < gameGrid.length; i++) {
            for (int j = 0; j < gameGrid[0].length; j++) {
                markGameGrid(Players.NO_ONE, i, j);
            }
        }
    }

    /**
     * Remets à zéro les données du jeu.
     */
    public void resetGame(){
        resetGameGrid();
        marks = 0;
        isPlayer1Turn = true;
        winner = Players.NO_ONE;
        gameOver = false;
    }

    /**
     * Passe au tour du prochain joueur.
     */
    public void changeTurn(){
        isPlayer1Turn = !isPlayer1Turn;
    }

    //region Parcelable Code
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (isPlayer1Turn ? 1 : 0));
        parcel.writeInt(marks);
        for (int j = 0; j < 3; j++) {
            parcel.writeArray(gameGrid[j]);
        }
        parcel.writeByte((byte) (gameOver ? 1 : 0));
    }

    protected GameData(Parcel in) {
        isPlayer1Turn = in.readByte() != 0;
        marks = in.readInt();
        gameGrid = new Players[3][3];
        for (int i = 0; i < 3; i++) {
            gameGrid[i] = (Players[]) in.readArray((ClassLoader) GameData.CREATOR);
        }
        gameOver = in.readByte() != 0;
    }

    public static final Creator<GameData> CREATOR = new Creator<GameData>() {
        @Override
        public GameData createFromParcel(Parcel in) {
            return new GameData(in);
        }

        @Override
        public GameData[] newArray(int size) {
            return new GameData[size];
        }
    };
    //endregion
}
