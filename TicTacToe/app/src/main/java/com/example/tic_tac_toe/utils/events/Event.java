package com.example.tic_tac_toe.utils.events;

/**
 * Évènement permettant au view model de communiquer avec une activité.
 * Source: https://stackoverflow.com/a/56072658
 */
public class Event<T> {
    private boolean hasBeenHandled = false;
    private final T content;

    public Event(T content) {
        this.content = content;
    }

    public T getContentIfNotHandled() {
        if (hasBeenHandled) {
            return null;
        } else {
            hasBeenHandled = true;
            return content;
        }
    }

    public boolean isHandled() {
        return hasBeenHandled;
    }
}
